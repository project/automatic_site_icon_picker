<?php

namespace Drupal\automatic_site_icon_picker\Plugin\Field\FieldFormatter;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Theme\ThemeManager;
use Drupal\Core\Url;
use Drupal\file\FileRepository;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\MimeTypes;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "automatic_site_icon_picker_link",
 *   label = @Translation("Link (favicon)"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class AutomaticSiteIconPickerLinkFormatter extends FormatterBase {
  const CONTENT_TYPES = [
    // Suggested by IANA.
    'application/ico',
    'application/octet-stream',
    'image/vnd.microsoft.icon',
    'image/ico',
    'image/icon',
    'image/x-icon',
    'image/mask-icon',
    'text/ico',
    'text/plain',
    // Suggested by W3C.
    'image/gif',
    'image/png',
  ];
  // Maximum content size.
  const MAX_BYTES = 1048576;

  /**
   * Construct a MyFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   Defines an interface for entity field definitions.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Request settings.
   * @param \Drupal\file\FileRepository $file_service
   *   Files.
   * @param \Drupal\Core\File\FileSystem $file_system
   *   Working with files.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   DB logger.
   * @param \Drupal\Core\Theme\ThemeManager $themes
   *   Themes.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduels
   *   Modulse.
   */
  public function __construct($plugin_id,
                              $plugin_definition,
                              FieldDefinitionInterface $field_definition,
                              array $settings,
    $label,
    $view_mode,
                              array $third_party_settings,
                              ClientInterface $http_client,
                              FileRepository $file_service,
                              FileSystem $file_system,
                              LoggerChannelFactory $logger,
                              ThemeManager $themes,
                              ModuleExtensionList $moduels) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->http_client = $http_client;
    $this->file_service = $file_service;
    $this->file_system = $file_system;
    $this->logger = $logger->get('Social media icon');
    $this->themes = $themes;
    $this->modules = $moduels;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      // Add any services you want to inject here.
      $container->get('http_client'),
      $container->get('file.repository'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('theme.manager'),
      $container->get('extension.list.module'),
    );
  }

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    if (!file_exists('public://social-media-icons/')) {
      $this->file_system->mkdir('public://social-media-icons/');
    }
    $current_theme_path = $this->themes->getActiveTheme()->getPath();
    $current_module_path = $this->modules->getPath('automatic_site_icon_picker');
    if (file_exists($current_theme_path . '/images/icons/default_social_link.svg')) {
      $icon_file_default = $current_theme_path . '/images/icons/default_social_link.svg';
    }
    else {
      $icon_file_default = $current_module_path . '/icons/default_social_link.svg';
    }

    $view_mode = $this->getSettings()['social_media_link'];
    $icon_size = $this->getSettings()['social_media_size'];
    $icon_sizes = [
      'large'  => '64',
      'medium' => '32',
      'small'  => '16',
    ];
    foreach ($items as $delta => $item) {
      $icon_file = $icon_file_default;
      // Select hostname from url "www.instagram.com".
      if (str_starts_with($item->uri, 'internal:') || str_starts_with($item->uri, 'entity:')) {
        $item->uri = Url::fromUri($item->uri, ['absolute' => TRUE])->toString();
      }
      $hostname = parse_url($item->uri, PHP_URL_HOST) ?? '';
      $host = Html::escape(str_replace('.', '-', $hostname));
      $not_found = FALSE;
      try {
        // DOC https://github.com/faviconkit/javascript-api.
        // https://MY-FAVICONKIT-ACCOUNT.faviconkit.com/HOST/SIZE.
        $data = $this->http_client->get("drunk-indigo-goose.faviconkit.com/$hostname/$icon_sizes[$icon_size]");
        $file_type = $data->getHeader('content-type')[0];
        if (strpos($file_type, ';')) {
          // Isset other parameters "image/x-icon; charset=UTF-8; ...".
          $file_type = substr($file_type, 0, strpos($file_type, ';'));
        }
        $image_format = MimeTypes::getDefault()->getExtensions($file_type);

        if (!in_array($file_type, self::CONTENT_TYPES) || $data->getBody()->getSize() > self::MAX_BYTES) {
          $not_found = TRUE;
        }

      }
      catch (\Exception $e) {
        $not_found = TRUE;
      }
      // Url doesn't global or no correct(not found).
      if (!parse_url($item->uri, PHP_URL_HOST) || $not_found) {
        $elements[$delta] = [
          '#theme' => 'automatic_site_icon_picker',
          '#url' => ['uri' => Url::fromUri($item->uri)],
          '#hosts' => NULL,
          '#icon_file' => $icon_file,
          '#elem' => $items[$delta],
          '#view_mode' => $view_mode,
          '#icon_size' => $icon_sizes[$icon_size],
        ];
        continue;
      }

      $icon_file = "public://social-media-icons/$host-$icon_sizes[$icon_size]px.$image_format[0]";
      if (file_exists($icon_file)) {
        $image_size = getimagesize($icon_file);
        $elements[$delta] = [
          '#theme' => 'automatic_site_icon_picker',
          '#url' => $item->getUrl(),
          '#hosts' => $host,
          '#icon_file' => $icon_file,
          '#elem' => $items[$delta],
          '#view_mode' => $view_mode,
          '#icon_size' => (!empty($image_size['0']) && $image_size['0'] >= $icon_sizes[$icon_size]) ? $icon_sizes[$icon_size] : NULL,
        ];
      }
      else {
        try {
          $handle = fopen($icon_file, 'w');
          fwrite($handle, $data->getBody());
          fclose($handle);
          $image_size = getimagesize($icon_file);
          $elements[$delta] = [
            '#theme' => 'automatic_site_icon_picker',
            '#url' => $item->getUrl(),
            '#hosts' => $host,
            '#icon_file' => $icon_file,
            '#elem' => $items[$delta],
            '#view_mode' => $view_mode,
            '#icon_size' => (!empty($image_size['0']) && $image_size['0'] >= $icon_sizes[$icon_size]) ? $icon_sizes[$icon_size] : NULL,
          ];
        }
        catch (\Exception $e) {
          $this->logger->error('@code: @message', [
            '@code' => $e->getCode(),
            '@message' => $e->getMessage(),
          ]);
        }
      }
    }
    return $elements;
  }

  /**
   * Static function DefaultSettings().
   */
  public static function defaultSettings() {
    return [
      'social_media_link' => 'icon_only',
      'social_media_size' => 'medium',
    ] + parent::defaultSettings();
  }

  /**
   * Static function settingsForm().
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);
    $form['social_media_link'] = [
      '#title' => $this->t('Display view'),
      '#type' => 'select',
      '#options' => [
        'icon_only' => $this->t('Icon only'),
        'icon_and_link' => $this->t('Icon and URL'),
        'icon_and_title' => $this->t('Icon and Title'),
      ],
      '#default_value' => $this->getSetting('social_media_link'),
    ];
    $form['social_media_size'] = [
      '#title' => $this->t('Icon size'),
      '#type' => 'select',
      '#options' => [
        'large' => $this->t('64px'),
        'medium' => $this->t('32px'),
        'small' => $this->t('16px'),
      ],
      '#default_value' => $this->getSetting('social_media_size'),
    ];

    return $form;

  }

}
